class VirtualMachine:

    # Initialization for default case
    def __init__(self):
        self.name = None
        self.number_of_CPUs = None
        self.RAM = None
        self.disk_size = None
        self.OS = None

    # getters and setters for every attribute
    def set_name(self, name):
        self.name = name

    def get_name(self):
        return self.name

    def set_CPUs(self, num):
        self.number_of_CPUs = num

    def get_CPUs(self):
        return self.number_of_CPUs

    def set_RAM(self, ram):
        self.RAM = ram

    def get_RAM(self):
        return self.RAM

    def set_disk_size(self, size):
        self.disk_size = size

    def get_disk_size(self):
        return self.disk_size

    def set_OS_type(self, type):
        self.OS = type

    def get_OS_type(self):
        return self.OS


# Use this space to create/manipulate Virtual Machine Objects:
VM = VirtualMachine()
VM.set_disk_size(5)
VM.set_OS_type("Linux")
VM.set_CPUs(4)
VM.set_name("Bob")
VM.set_RAM(16)
print(VM.get_disk_size())
print(VM.get_OS_type())
print(VM.get_CPUs())
print(VM.get_name())
print(VM.get_RAM())
